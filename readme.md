# Flask Sqlite3 CRUD demo
Someone recently asked me how to get started with Flask, 
beyond the very basic tutorials but simpler than the "intermediate" tutorials.


I had two hours of downtime, so here's a little "minimal working example" 
on how to perform simple CRUD operations in a sqlite3 database from a Flask site.

# Using the demo
To just run the demo and see what happens do the following:
(I'm assuming a python >3.10 environment is available)

## 0: Clone this repo:
Start by cloning this repo
```bash
git clone https://github.com/Veticus/Flask_sqlite3_CRUD_demo
```
and enter the directory
```bash
cd Flask_sqlite3_CRUD_demo
```

## 1: Create (and activate) a virtual environment
Open your favourite terminal application hammer these in:

```bash
python3.11 -m venv env
source env/bin/activate.sh
```
Note: You might need to replace `python3.11` with 
or whatever version you have installed.
Note: If you're, like me, using the fish shell, just replace the `.sh` with `.fish`.

## 2: Installing required packages
```bash
pip install -r requirements.txt
```

## 3: Running the demo
```bash
export FLASK_APP=app.py
flask run
```
Then open your browser and go to `http://localhost:5000`



# How to make this happen yourself
A little more fun is seing the parts come together. This should also give more insight into what's happening.

## 0: Prerequisites
Start, like above, by creating a fresh directory, a new virtual environment and 
install `Flask` and `Flask-SQLAlchemy` using `pip install`.

`Flask` will do the "website stuff" and `FLASK-SQLAlchemy` will "do all the database stuff".

```bash
mkdir my_flask_sqlite3_scrum_demo
cd my_flask_sqlite3_scrum_demo
python3.11 -m venv env
source env/bin/activate.sh
pip install Flask Flask-SQLAlchemy
```


Here's a quick overview of the file we'll be creating in the following:
```
my_flask_sqlite3_scrum_demo/
├── templates/
│   ├── index.html
│   └── base.html
├── app.py
└── models.py
```

## 1: The Flask app itself
Create a file called `app.py` in the directory. 
(This should be the same directory your virtual environment is in.)
Open it with your favourite editor and throw this in:
```python
from flask import Flask, request, render_template, redirect, url_for
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///site.db'
db = SQLAlchemy(app)

from models import Entry
```

The top three lines are all the libraries we're going to use.
Lines five to seven make a new Flask object, set a config variable that SQLAlchemy will use later and 
create a SQLAlchemy object that will work with the Flask object.

The last line imports the database model we're creating in the next step.
This needs to be _after_ the Flask app and SQLAlchemy object have been created.
This is ugly, but unfortunately necessary.

### Potential trap here
If you get a `OperationalError` when running flask, this might be because this line is misplaced.
Just verify that the `from models import Entry` line is after the creation of the Flask app and SQLAlchem instance.
Then repeat step 3.
(Don't worry think too much about this part).

## 2: The "database model"
Create a file called `models.py` in the same directory as above, and throw this in:
```python
from app import db

class Entry(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    content = db.Column(db.String(200), nullable=False)
    date_posted = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)

    def __repr__(self):
        return f"Entry('{self.content}', '{self.date_posted}')"
```

This is where we define what the data in the database should look like.
Don't worry too much about the details yet.

## 3: Creating the database itself
Open your terminal and launch a Flask Shell by running the `flash shell` command.
Then run these three commands in it:
```python
db.create_all()
quit()
```
This will create the database file in the `instance/` directory.

Note: Some tutorials still want you to do this in a python REPL, 
but as of `Flask-SQLAlchemy` version 3 this won't work anymore.
(Source: https://stackoverflow.com/questions/73961938/flask-sqlalchemy-db-create-all-raises-runtimeerror-working-outside-of-applicat)
Again, don't worry about why.

## 4: Define routes
Head back to the `app.py` file and throw this at these two at the bottom:

```python
@app.route("/", methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        entry_content = request.form['content']
        if entry_content:
            new_entry = Entry(content=entry_content)
            db.session.add(new_entry)
            db.session.commit()
            return redirect(url_for('index'))
    entries = Entry.query.order_by(Entry.date_posted.desc()).all()
    return render_template('index.html', entries=entries)
```

```python
@app.route("/edit/<int:entry_id>", methods=['POST'])
def edit_entry(entry_id):
    entry = Entry.query.get_or_404(entry_id)
    entry.content = request.form['content']
    db.session.commit()
    return redirect(url_for('index'))
```

This tells Flask what to do with the requests from the browser.

## 5: Create the templates
Start by creating a directory called `templates` and enter it.
In there create a new file and call it `base.html`.

For now just throw this snippet in the file:
```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{% block title %}{% endblock %}</title>
</head>
<body>
    {% block content %}{% endblock %}
</body>
</html>
```
The stuff in curly brackets is Jinja2 syntax, that i mentioned last time we talked.

Now create a file called `index.html` in the `templates/` directory and throw this in:
```html
{% extends 'base.html' %}
{% block title %}Entries{% endblock %}
{% block content %}
    <h1>Entries</h1>
    <form action="/" method="POST">
        <input type="text" name="content" placeholder="Enter new text">
        <input type="submit" value="Submit">
    </form>
    {% for entry in entries %}
        <form action="/edit/{{ entry.id }}" method="POST">
            <textarea name="content">{{ entry.content }}</textarea>
            <button type="submit">Save</button>
            <small>{{ entry.date_posted.strftime('%Y-%m-%d %H:%M') }}</small>
        </form>
    {% endfor %}
{% endblock %}
```

Don't worry too much about what the Jinja2 syntax does. We'll get into that, when we meet up.

## 6: Run the Flask app
In your terminal start by setting an environment variable called `FLASK_APP` and set it to `app.py`.
Then run it using flask itself.

```bash
export FLASK_APP=app.py
flask run
```
