from flask import Flask, request, render_template, redirect, url_for
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///site.db'
db = SQLAlchemy(app)

# Note that this isn't good python style. But it's necessary here.
# There's a risk of circular imports, if the model is imported before the
# Flask app and SQLAlchemy object have been initialised.
from models import Entry


@app.route("/", methods=['GET', 'POST'])
def index():
	if request.method == 'POST':
		entry_content = request.form['content']
		if entry_content:
			new_entry = Entry(content=entry_content)
			db.session.add(new_entry)
			db.session.commit()
			return redirect(url_for('index'))
	entries = Entry.query.order_by(Entry.date_posted.desc()).all()
	return render_template('index.html', entries=entries)


@app.route("/edit/<int:entry_id>", methods=['POST'])
def edit_entry(entry_id):
	entry = Entry.query.get_or_404(entry_id)
	entry.content = request.form['content']
	db.session.commit()
	return redirect(url_for('index'))
